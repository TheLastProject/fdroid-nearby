package org.fdroid.fdroid.net;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public abstract class Downloader {

    public static final String ACTION_INTERRUPTED = "org.fdroid.fdroid.net.Downloader.action.INTERRUPTED";

    public static final String EXTRA_ERROR_MESSAGE = "org.fdroid.fdroid.net.Downloader.extra.ERROR_MESSAGE";
}
